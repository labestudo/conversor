"use strict"

const readlineSync = require('readline-sync');
const puppeteer = require('puppeteer');


async function robo(){
    
    const browser = await puppeteer.launch({headless: true});
    const page = await browser.newPage();
    const askInit = readlineSync.question('Me diga qual moeda deseja converter: ')
    const askEnd = readlineSync.question('Agora me fale, para qual moeda deseja fazer a conversão: ')
    const coinInit = (askInit || `dolar`);
    const coinEnd =(askEnd || `real`);
    await page.goto(`https://www.google.com/search?sxsrf=ALeKk01YcKDXiBzVynH1YLpAMKB9wfNPbg%3A1615549044343&ei=dFJLYMvAFLDC5OUPu9-nyAg&q=${coinInit}+para+${coinEnd}&oq=${coinInit}+para+${coinEnd}&gs_lcp=Cgdnd3Mtd2l6EAMyBQgAEMsBMgUIABDLATIHCAAQChDLATIHCAAQChDLATIFCAAQywEyBQgAEMsBMgcIABAKEMsBMgcIABAKEMsBMgcIABAKEMsBMgUIABDLAToHCAAQsAMQQzoHCAAQRxCwAzoECCMQJzoECAAQQzoCCAA6CAguEMcBEK8BOgUIABCRAjoHCAAQhwIQFDoICC4QxwEQowI6CQgjECcQRhCCAlC6N1joR2CgSWgBcAJ4AIABrQGIAdIPkgEEMC4xNZgBAKABAaoBB2d3cy13aXrIAQnAAQE&sclient=gws-wiz&ved=0ahUKEwjLyp6X1arvAhUwIbkGHbvvCYkQ4dUDCA0&uact=5`);
    // await page.click('#ZEB7Fb vk_gy vk_sh Hg3mWc');
    const resultado = await page.evaluate(() => {
       return document.querySelector ('.a61j6.vk_gy.vk_sh.Hg3mWc').value;
    });
    console.log (`O valor da moeda ${coinInit} em ${coinEnd} é ${resultado}`);


}
robo();